#!/usr/bin/env python

import lucene

# from org.apache.lucene.search import IndexSearcher, TermQuery, MatchAllDocsQuery
from org.apache.lucene.index import (
    DirectoryReader,
)
from org.apache.lucene.store import MMapDirectory, FSDirectory
from java.nio.file import Paths
from org.apache.lucene.search import IndexSearcher, TermQuery, MatchAllDocsQuery

# import argparse to recover the query from the command line
import argparse
import os, glob
from tqdm import tqdm

# from org.apache.lucene.search.similarities import BM25Similarity, ClassicSimilarity, LMDirichletSimilarity, TFIDFSimilarity
from org.apache.lucene.search.similarities import (
    BM25Similarity,
    ClassicSimilarity,
    LMDirichletSimilarity,
)
from org.apache.lucene.queryparser.classic import QueryParser
from org.apache.lucene.analysis.standard import StandardAnalyzer

from extract_doc import (
    remove_stop_words_in_text,
    segment_words,
    Topic,
    extract_topics_from_text_file,
)
from config_properties import EvaluationFormula, SegmentationType


class IRSearcher:
    """
    This class will search for documents in an index and make the evaluation.
    Cette classe va rechercher des documents dans un index et faire l'evaluation.

     Les parametres de la classe sont:

        - index_dir: le repertoire ou est cree l'index        - result_file: le fichier qui contiendra les resultats de la recherche
        - short_query: True si on veut utiliser les requetes courtes sinon False
        - evaluation_formula: la formule d'evaluation a utiliser pour evaluer le systeme de recherche comme TF-IDF, BM25, ... par defaut on utilise TF-IDF
        - stops_words_usage: True si on veut utiliser les mots vides sinon False
        - segmentation_type: le type de segmentation a utiliser pour segmenter les mots comme lemmatization, stemming, ... par defaut on utilise lemmatization
        - lower_case_usage: True si on veut utiliser la mise en minuscule sinon False
        - result_file: le fichier qui contiendra les resultats de la recherche


    """

    def __init__(
        self,
        index_dir,
        result_file,
        short_query: bool = False,
        evaluation_formula: EvaluationFormula = EvaluationFormula.TFIDFSimilarity,
        stops_words_usage: bool = True,
        segmentation_type: SegmentationType = SegmentationType.Lematization,
        lower_case_usage: bool = True,
    ):
        self.index_dir = index_dir
        self.result_file = result_file
        self.short_query = short_query
        self.evaluation_formula = evaluation_formula
        self.stops_words_usage = stops_words_usage
        self.segmentation_type = segmentation_type
        self.lower_case_usage = lower_case_usage

        lucene.initVM()
        index_dir_store = MMapDirectory(Paths.get(self.index_dir))
        reader = DirectoryReader.open(index_dir_store)
        self.searcher = IndexSearcher(reader)

        # Definition du schema de ponderation
        if self.evaluation_formula == EvaluationFormula.TFIDFSimilarity:
            # The  TF-IDF similarity used in Lucene is the classic similarity, which defines the tf(t in d) as sqrt(freq).
            self.searcher.setSimilarity(ClassicSimilarity())

            pass
        elif self.evaluation_formula == EvaluationFormula.BM25Similarity:
            self.searcher.setSimilarity(BM25Similarity())
            print("\n\n\n Using the BM25Similarity\n\n\n")
            # exit()

        elif self.evaluation_formula == EvaluationFormula.ClassicSimilarity:
            self.searcher.setSimilarity(ClassicSimilarity())
            print("\n\n\n Using the ClassicSimilarity\n\n\n")
        elif self.evaluation_formula == EvaluationFormula.LMDirichletSimilarity:
            self.searcher.setSimilarity(LMDirichletSimilarity())
            print("\n\n\n Using the LMDirichletSimilarity\n\n\n")

    def search(self, query_no, query: str):
        """
        Cette fonction permet de faire une recherche dans l'index
        et de donner des resultats dans le fichier result_file
        """

        ######## Pretraitement de la requete

        # si on doit retirer les stops words
        # if self.stops_words_usage:
        #     query = remove_stop_words_in_text(query)
        # # si on doit segmenter les mots
        # if self.segmentation_type != SegmentationType.NoneSegmentation:
        #     query = segment_words(query, self.segmentation_type)

        # if self.lower_case_usage:
        #     query = query.lower()

        analyzer = StandardAnalyzer(
            # Version.LUCENE_30
        )

        query_parser = QueryParser(
            "FULL_CONTENT", analyzer
        )  # On va chercher dans le champ text et si on veut checher dans tous les champs on met None comme par exemple: query_parser = QueryParser(None, analyzer)
        query = query_parser.parse(
            QueryParser.escape(query)
        )  # ceci permet d'echapper les caracteres speciaux

        # term_query = TermQuery(Term(query))

        hits = self.searcher.search(query, 1000)

        with open(
            self.result_file, "a"
        ) as f:  # si le fichier n'existe pas, il sera cree
            for rank, hit in enumerate(hits.scoreDocs, start=1):
                doc = self.searcher.doc(hit.doc)
                doc_no = doc.get("DOCNO").strip().upper()
                # if the segmentation type is different from NoneSegmentation, we will use the field FULL_CONTENT
                # Normally, we are suposse to not preprocess the doc_no
                if self.segmentation_type != SegmentationType.NoneSegmentation:
                    split_content = doc.get("FULL_CONTENT").strip().split(" ")
                    doc_no = split_content[0] + split_content[1] + split_content[2]
                    doc_no = doc_no.upper()
                    # print(f"\n\nDOCNO:    {doc_no}\n\n ")

                    # exit()

                f.write(
                    f"{int(query_no)} 0 {doc_no} {rank} {hit.score} DK_LUCENE_STANDARD_SYSTEM\n"  # Nous ecrivons le numero de la requete en chiffre car c'est le format de la collection TREC de resultats reeles
                )  # STANDARD est le nom du run

        return hits


def main():
    """
        un exemple d'appel du programme est:
           time python searcher.py \
             --evaluation_formula BM25 \
             --short_query \
             --stops_words_usage \
                --segmentation_type Lematization \
                --lower_case_usage 
                
                
                ########################  TF-IDF  ##############################
                       -----------------  SANS PRETRAITEMENT  -----------------
                           ______________SHORT QUERY______________
                             *time python searcher.py \
                                --query_dir TREC_AP_88-90/Topics-requetes/ \
                                --evaluation_formula TF-IDF \
                                --segmentation_type NoneSegmentation \
                                --short_query
                           __________________LONG QUERY________________
    
                              time python searcher.py \
                                --query_dir TREC_AP_88-90/Topics-requetes/ \
                                --evaluation_formula TF-IDF \
                                --segmentation_type NoneSegmentation
                                            

                        
                        
                        -----------------  AVEC PRETRAITEMENT: Stemming  -----------------
                            __________________SHORT QUERY________________
                                 time python searcher.py \
                                    --query_dir TREC_AP_88-90/Topics-requetes/ \
                                    --segmentation_type Stemming \
                                    --stops_words_usage \
                                    --lower_case_usage \
                                    --short_query
                                                
                            __________________LONG QUERY________________
                                    time python searcher.py \
                                    --query_dir TREC_AP_88-90/Topics-requetes/ \
                                    --evaluation_formula TF-IDF \
                                    --segmentation_type Stemming \
                                    --stops_words_usage \
                                    --lower_case_usage
                                                            
                                                    
                
                
                
                ########################  BM25  ##############################
                          -----------------  SANS PRETRAITEMENT  -----------------
                            ______________SHORT QUERY______________
                                time python searcher.py \
                                    --query_dir TREC_AP_88-90/Topics-requetes/ \
                                    --evaluation_formula BM25 \
                                    --segmentation_type NoneSegmentation \
                                    --short_query


                            __________________LONG QUERY________________
     
                                time python searcher.py \
                                    --query_dir TREC_AP_88-90/Topics-requetes/ \
                                    --evaluation_formula BM25 \
                                    --segmentation_type NoneSegmentation
                                              
    
                            
                            
                            -----------------  AVEC PRETRAITEMENT Stemming   -----------------
                            __________________SHORT QUERY________________
                                time python searcher.py \
                                    --query_dir TREC_AP_88-90/Topics-requetes/ \
                                            --evaluation_formula BM25 \
                                                --segmentation_type Stemming \
                                                    --short_query \
                                                        --stops_words_usage \
                                                            --lower_case_usage
                                                            
                            __________________LONG QUERY________________
                                      time python searcher.py \
                                        --query_dir TREC_AP_88-90/Topics-requetes/ \
                                                --evaluation_formula BM25 \
                                                    --segmentation_type Stemming \
                                                        --stops_words_usage \
                                                            --lower_case_usage
                                                            
                            
                            
                ########################  LMDirichletSimilarity  ##############################
                          -----------------  SANS PRETRAITEMENT  -----------------
                            ______________SHORT QUERY______________
                                time python searcher.py \
                                    --query_dir TREC_AP_88-90/Topics-requetes/ \
                                    --evaluation_formula LMDirichletSimilarity \
                                    --segmentation_type NoneSegmentation \
                                    --short_query
                                                    
                            __________________LONG QUERY________________
                                
                                     time python searcher.py \
                                        --query_dir TREC_AP_88-90/Topics-requetes/ \
                                                --evaluation_formula LMDirichletSimilarity \
                                                    --segmentation_type NoneSegmentation
                        
                        -----------------  AVEC PRETRAITEMENT Stemming   -----------------
                            __________________SHORT QUERY________________
                                     time python searcher.py \
                                        --query_dir TREC_AP_88-90/Topics-requetes/ \
                                                --evaluation_formula LMDirichletSimilarity \
                                                    --segmentation_type Stemming \
                                                        --short_query \
                                                            --stops_words_usage \
                                                                --lower_case_usage
                            __________________LONG QUERY________________
                                     time python searcher.py \
                                        --query_dir TREC_AP_88-90/Topics-requetes/ \
                                                --evaluation_formula LMDirichletSimilarity \
                                                    --segmentation_type Stemming \
                                                        --stops_words_usage \
                                                            --lower_case_usage
                                                                
                                                                
                                                                
                                                                
                                                                
                                               
    """
    # Nous recuperons les arguments passes en ligne de commande creer l'onjet IRSearcher
    parser = argparse.ArgumentParser()
    parser.add_argument("--query_dir", help="the query driectory", type=str)
    parser.add_argument(
        "--short_query", help="if we should use short queries", action="store_true"
    )
    parser.add_argument(
        "--evaluation_formula",
        help="the evaluation formula to use",
        type=str,
        default="TF-IDF",
    )
    parser.add_argument(
        "--stops_words_usage", help="if we should use stop words", action="store_true"
    )
    parser.add_argument(
        "--segmentation_type",
        help="the segmentation type to use",
        type=str,
        default="NoneSegmentation",
    )
    parser.add_argument(
        "--lower_case_usage", help="if we should use lower case", action="store_true"
    )

    args = parser.parse_args()

    # Le nom du repertoire de l'index et le nom du fichier de resultat seront calcules en fonction des arguments car l'index est cree avec les memes arguments
    # Le format est   index_{evaluation_formula}_{segmentation_type}_{stops_words_usage}_{lower_case_usage}
    # print(f"Stop words usage: {args.stops_words_usage}\n")
    evaluation_formula_str = args.evaluation_formula
    segmentation_type_str = args.segmentation_type
    stops_words_usage_str = (
        "remove-stop-words" if args.stops_words_usage else "keep-stop-words"
    )
    lower_case_usage_str = (
        "use-lower-case" if args.lower_case_usage else "no-lower-case"
    )
    short_query_str = "short-query" if args.short_query else "long-query"
    index_dir = f"indexation/index_{evaluation_formula_str}_{segmentation_type_str}_{stops_words_usage_str}_{lower_case_usage_str}"
    result_file = f"results/results_{evaluation_formula_str}_{segmentation_type_str}_{stops_words_usage_str}_{lower_case_usage_str}_{short_query_str}.txt"

    # chek if the index directory exists
    if not os.path.exists(index_dir):
        raise Exception(f"The index directory {index_dir} does not exist")
    else:
        print(f"Using index directory: {index_dir}")

    ir_searcher = IRSearcher(
        index_dir=index_dir,
        result_file=result_file,
        short_query=args.short_query,
        evaluation_formula=EvaluationFormula.from_str(evaluation_formula_str),
        stops_words_usage=args.stops_words_usage,
        segmentation_type=SegmentationType.from_str(args.segmentation_type),
        lower_case_usage=args.lower_case_usage,
    )

    number_of_processed_topics = 0
    for topic_filename in tqdm(
        sorted(glob.glob(os.path.join(args.query_dir, "*.txt")))
    ):
        print(f"\n\n\n\n\nProcessing the file: {topic_filename}\n\n\n\n\n")
        #    On parcourt tous les fichiers de requetes
        # queries_topic: list[Topic] = []

        queries_topic = extract_topics_from_text_file(topic_filename)
        for topic in tqdm(queries_topic):
            # pour chaque requete, on recupere le numero de la requete et la requete
            query_no = topic.num
            query_string = topic.query(
                short_query=ir_searcher.short_query,
                remove_stop_words=ir_searcher.stops_words_usage,
                segmentationType=ir_searcher.segmentation_type,
                lower_case=ir_searcher.lower_case_usage,
            )

            ir_searcher.search(query_no, query_string)
            number_of_processed_topics += 1


    # queries_topic = extract_topics_from_text_file("TREC_AP_88-90/Topics-requetes/topics.1-50.txt")
    # for topic in tqdm(queries_topic):
    #     # pour chaque requete, on recupere le numero de la requete et la requete
    #     query_no = topic.num
    #     query_string = topic.query(
    #         short_query=ir_searcher.short_query,
    #         remove_stop_words=ir_searcher.stops_words_usage,
    #         segmentationType=ir_searcher.segmentation_type,
    #         lower_case=ir_searcher.lower_case_usage,
    #     )

    #     ir_searcher.search(query_no, query_string)
    #     number_of_processed_topics += 1

    # queries_topic = extract_topics_from_text_file("TREC_AP_88-90/Topics-requetes/topics.51-100.txt")
    # for topic in tqdm(queries_topic):
    #     # pour chaque requete, on recupere le numero de la requete et la requete
    #     query_no = topic.num
    #     query_string = topic.query(
    #         short_query=ir_searcher.short_query,
    #         remove_stop_words=ir_searcher.stops_words_usage,
    #         segmentationType=ir_searcher.segmentation_type,
    #         lower_case=ir_searcher.lower_case_usage,
    #     )

    #     ir_searcher.search(query_no, query_string)
    #     number_of_processed_topics += 1

    # queries_topic = extract_topics_from_text_file("TREC_AP_88-90/Topics-requetes/topics.101-150.txt")
    # for topic in tqdm(queries_topic):
    #     # pour chaque requete, on recupere le numero de la requete et la requete
    #     query_no = topic.num
    #     query_string = topic.query(
    #         short_query=ir_searcher.short_query,
    #         remove_stop_words=ir_searcher.stops_words_usage,
    #         segmentationType=ir_searcher.segmentation_type,
    #         lower_case=ir_searcher.lower_case_usage,
    #     )

    #     ir_searcher.search(query_no, query_string)
    #     number_of_processed_topics += 1

    # close the index searcher
    ir_searcher.searcher.getIndexReader().close()
    print(f"\n\n\n\n\nNumber of processed topics: {number_of_processed_topics}")


if __name__ == "__main__":
    main()
