import sys, lucene, threading, time, re, glob, os
from datetime import datetime
from tqdm import tqdm  # Ce package permet d'afficher une barre de progression
import asyncio
import argparse
from java.nio.file import Paths
from org.apache.lucene.analysis.miscellaneous import LimitTokenCountAnalyzer
from org.apache.lucene.analysis.standard import StandardAnalyzer
from org.apache.lucene.document import Document, Field, FieldType
from org.apache.lucene.index import (
    FieldInfo,
    IndexWriter,
    IndexWriterConfig,
    IndexOptions,
)
from org.apache.lucene.store import MMapDirectory, FSDirectory


from org.apache.lucene.index import (
    IndexWriter,
    IndexReader,
    DirectoryReader,
    Term,
    IndexWriterConfig,
)
from org.apache.lucene.search import IndexSearcher, TermQuery, MatchAllDocsQuery
from org.apache.lucene.search.similarities import (
    BM25Similarity,
    ClassicSimilarity,
    LMDirichletSimilarity,
    TFIDFSimilarity,
)
from org.apache.lucene.queryparser.classic import QueryParser


# import the IRDocument class from extract_doc.py
from extract_doc import (
    extract_docs,
    remove_stop_words_in_text,
    segment_words,
    IRDocument,
    read_IRDocuments_from_pickle_file,
)

from config_properties import EvaluationFormula, SegmentationType


class IR:
    """
    This class will index all the document and create a search engine
     to search for documents and make the evaluation.
     Cette classe va indexer tous les documents et creer un moteur de recherche
        pour rechercher des documents et faire l'evaluation.

     Les parametres de la classe sont:

        - docdir: le repertoire qui contient les documents a indexer
        - index_dir: le repertoire ou sera cree l'index
        - evaluation_formula: la formule d'evaluation a utiliser pour evaluer le systeme de recherche comme TF-IDF, BM25, ... par defaut on utilise TF-IDF
        - stops_words_usage: True si on veut utiliser les mots vides sinon False
        - segmentation_type: le type de segmentation a utiliser pour segmenter les mots comme lemmatization, stemming, ... par defaut on utilise lemmatization
        - lower_case_usage: True si on veut utiliser la mise en minuscule sinon False
        - spell_checking_usage: True si on veut utiliser la correction orthographique sinon False
        - result_file: le fichier qui contiendra les resultats de la recherche


    """

    def __init__(
        self,
        docdir,
        index_dir,
        evaluation_formula=EvaluationFormula.TFIDFSimilarity,
        stops_words_usage=True,
        segmentation_type=SegmentationType.Lematization,
        lower_case_usage=True,
       
    ):
        self.docdir = docdir
        self.index_dir = index_dir

        self.evaluation_formula = evaluation_formula
        self.stops_words_usage = stops_words_usage
        self.segmentation_type = segmentation_type
        self.lower_case_usage = lower_case_usage

        lucene.initVM()
        index_dir_store = MMapDirectory(Paths.get(self.index_dir))
        analyzer = StandardAnalyzer(
            # Version.LUCENE_30
        )
        index_writer_config = IndexWriterConfig(analyzer)
        # utilisation de la formule d'evaluation
        if self.evaluation_formula == EvaluationFormula.TFIDFSimilarity:
            # in lucene, the tf-idf is the is an abstract class and the class that represents the tf-idf is the ClassicSimilarity class
            index_writer_config.setSimilarity(ClassicSimilarity())

            print("\n\n\n\n Using TFIDFSimilarity \n\n\n\n")
        elif self.evaluation_formula == EvaluationFormula.BM25Similarity:
             index_writer_config = index_writer_config.setSimilarity(BM25Similarity())
            #  BM25 is the default similarity used in lucene: https://lucene.apache.org/core/7_6_0/core/org/apache/lucene/search/similarities/Similarity.html
             print("\n\n\n\n Using BM25Similarity \n\n\n\n")
            #  exit()            
          
        elif self.evaluation_formula == EvaluationFormula.LMDirichletSimilarity:
            index_writer_config = index_writer_config.setSimilarity(LMDirichletSimilarity())
            print("\n\n\n\n Using LMDirichletSimilarity \n\n\n\n")
        index_writer_config = index_writer_config.setOpenMode(
            IndexWriterConfig.OpenMode.CREATE
        )  # Ceci permet de dire que lorsqu'on va indexer, on va creer un nouvel index
        self.writer = IndexWriter(
            index_dir_store,
            index_writer_config,
        )

        self.indexer()

    def indexer(self):
        """
        Cette fonction permet d'indexer un repertoire de documents nome
        docdir et de creer l'index dans le repertoire index_dir

        """

       
        for file_name in tqdm(glob.glob(os.path.join(self.docdir, "*"))):
           
            docs = read_IRDocuments_from_pickle_file(file_name)
            print(f"Indexing {file_name}\n")

            for ir_doc in tqdm(docs):
                self.add_ir_doc_to_index(ir_doc)
                # On doit liberer la memoire
                del ir_doc

            del docs
        self.writer.close()

    def add_ir_doc_to_index(self, ir_doc: IRDocument):
        """
        This function add an IRDocument to the index
        :param ir_doc: the IRDocument to add

        """

        full_document_content = ir_doc.text
        current_doc = Document()
        field_type = FieldType()
        field_type.setStored(True)
        field_type.setIndexOptions(
            IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS
        )
        current_doc.add(
            Field(
                "FULL_CONTENT",
                full_document_content,
                field_type,
            ),
        )
        current_doc.add(
            Field(
                "DOCNO",
                ir_doc.DOCNO,
                field_type,
            ),
        )
        self.writer.addDocument(current_doc)
        # On doit liberer la memoire
        del current_doc
        del full_document_content
        del field_type

  


def indexing():
    """
    We will retreive from the command line the arguments:
    - the directory where the index will be created
    - the evaluation formula to use: TF-IDF, 1 for BM25,  LMDirichletSimilarity,  for ClassicSimilarity
    - the segmentation type to use: Lematization for ç, Stemming for stemming, NoneSegmentation for no segmentation
    - the usage of stop words: keep-stop-words for no usage, remove-stop-words for usage
    - the usage of lower case: no-lower-case for no usage, use-lower-case for usage
    for example:time  python3 indexing.py index TF-IDF Lematization keep-stop-words use-lower-case
                time  python3 indexing.py index BM25 NoneSegmentation remove-stop-words no-lower-case
          
          
                 #  pour avoir des noms de dossier index et se reperer, le format est le suivant:
                #  index_{evaluation_formula}_{segmentation_type}_{stops_words_usage}_{lower_case_usage}
                
                
                 Toutes les possibilites sont les suivantes:
                 
                 ----------------------------- TF-IDF ---------------------------------
                 
                                        ++++++++ SANS PRETRAITEMENTS ++++++++++++++
                 - sans pretraitements et TF-IDF:
                 
                 
                   


                       * time  python3 indexing.py \
                          index_TF-IDF_NoneSegmentation_keep-stop-words_no-lower-case \
                             TF-IDF \
                             NoneSegmentation \
                             keep-stop-words \
                             no-lower-case
                                        ++++++++ AVEC PRETRAITEMENTS ++++++++++++++
                 - avec lemmatization, sans stop words, avec lower caseet TF-IDF:
                  

                        
                      time  python3 indexing.py \
                            index_TF-IDF_Lematization_remove-stop-words_use-lower-case \
                            TF-IDF \
                            Lematization \
                            remove-stop-words \
                            use-lower-case
                       
                       docker run -it --privileged -d --name index_TF-IDF_Lematization_remove-stop-words_use-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                       docker exec  index_TF-IDF_Lematization_remove-stop-words_use-lower-case \
                         time  python3 /root/pylucene_workdir/indexing.py \
                            index_TF-IDF_Lematization_remove-stop-words_use-lower-case \
                            TF-IDF \
                            Lematization \
                            remove-stop-words \
                            use-lower-case
                            
                       
                        docker run -it --privileged -d --name pylucene coady/pylucene:latest 
                        docker run -it --privileged -d --name pylucene  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        docker exec -it pylucene bash

                        
                        
                        
                - avec Stemming, sans stop words, avec lower case et TF-IDF:

                        tmux new -s index_TF-IDF_Stemming_remove-stop-words_use-lower-case
                        mkdir index_TF-IDF_Stemming_remove-stop-words_use-lower-case
                        docker run -it --privileged -d --name index_TF-IDF_Stemming_remove-stop-words_use-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        docker run -it --privileged -d --name stemming_preprocessing  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        
                        docker exec -it index_TF-IDF_Stemming_remove-stop-words_use-lower-case bash
                        cd /root/pylucene_workdir
                        pip install -r requirements.txt
                        python -m spacy download en_core_web_sm
                        
                        
                       time  python3 indexing.py \
                            index_TF-IDF_Stemming_remove-stop-words_use-lower-case \
                            TF-IDF \
                            Stemming \
                            remove-stop-words \
                            use-lower-case
                        
                        --------------------------------- BM25 --------------------------------
                                       
                                        ++++++++ AVEC PRETRAITEMENTS ++++++++++++++

                 - avec lemmatization, sans stop words, avec lower case et BM25:
                        tmux new -s index_BM25_Lematization_remove-stop-words_use-lower-case
                        mkdir index_BM25_Lematization_remove-stop-words_use-lower-case
                        docker run -it --privileged -d --name index_BM25_Lematization_remove-stop-words_use-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest

                        docker exec -it index_BM25_Lematization_remove-stop-words_use-lower-case bash
                        cd /root/pylucene_workdir
                      time  python3 indexing.py \
                            index_BM25_Lematization_remove-stop-words_use-lower-case \
                            BM25 Lematization \
                            remove-stop-words \
                            use-lower-case
                            
                
                 - avec Stemming, sans stop words, avec lower case et BM25:
                        tmux new -s index_BM25_Stemming_remove-stop-words_use-lower-case
                        mkdir index_BM25_Stemming_remove-stop-words_use-lower-case
                        docker run -it --privileged -d --name index_BM25_Stemming_remove-stop-words_use-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        cd /root/pylucene_workdir
                        pip install -r requirements.txt
                        python -m spacy download en_core_web_sm
                        
                          time  python3 indexing.py \
                            index_BM25_Stemming_remove-stop-words_use-lower-case \
                            BM25 \
                            Stemming \
                            remove-stop-words \
                            use-lower-case
                            
                            
                                    ++++++++ SANS PRETRAITEMENTS ++++++++++++++
                        
                 - Sans lemmatization, avec stop words, sans lower case et BM25:
                        tmux new -s index_BM25_NoneSegmentation_keep-stop-words_no-lower-case
                        mkdir index_BM25_NoneSegmentation_keep-stop-words_no-lower-case
                        docker run -it --privileged -d --name index_BM25_NoneSegmentation_keep-stop-words_no-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        
                        docker exec -it index_BM25_NoneSegmentation_keep-stop-words_no-lower-case bash
                        cd /root/pylucene_workdir
                      time  python3 indexing.py \
                             index_BM25_NoneSegmentation_keep-stop-words_no-lower-case \
                             BM25 \
                             NoneSegmentation \
                             keep-stop-words \
                             no-lower-case
                             
                             
                             
                            -------------------- LMDirichletSimilarity-------------------------
                        
                        
                                        ++++++++ AVEC PRETRAITEMENTS ++++++++++++++
                        
                - avec lemmatization, sans stop words, avec lower case et LMDirichletSimilarity:
                        tmux new -s index_LMDirichletSimilarity_Lematization_remove-stop-words_use-lower-case
                        mkdir index_LMDirichletSimilarity_Lematization_remove-stop-words_use-lower-case
                        docker run -it --privileged -d --name index_LMDirichletSimilarity_Lematization_remove-stop-words_use-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        docker exec -it index_LMDirichletSimilarity_Lematization_remove-stop-words_use-lower-case bash
                        cd /root/pylucene_workdir
                      time  python3 indexing.py \
                           index_LMDirichletSimilarity_Lematization_keep-stop-words_use-lower-case \
                            LMDirichletSimilarity \
                            Lematization \
                            remove-stop-words \
                            use-lower-case
                            
                            
                - avec Stemming, sans stop words, avec lower case et LMDirichletSimilarity:
                        tmux new -s index_LMDirichletSimilarity_Stemming_remove-stop-words_use-lower-case
                        mkdir index_LMDirichletSimilarity_Stemming_remove-stop-words_use-lower-case
                        docker run -it --privileged -d --name index_LMDirichletSimilarity_Stemming_remove-stop-words_use-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        docker exec -it index_LMDirichletSimilarity_Stemming_remove-stop-words_use-lower-case bash
                        cd /root/pylucene_workdir
                        pip install -r requirements.txt
                        python -m spacy download en_core_web_sm
                        
                         time  python3 indexing.py \
                            index_LMDirichletSimilarity_Stemming_remove-stop-words_use-lower-case \
                            LMDirichletSimilarity \
                            Stemming \
                            remove-stop-words \
                            use-lower-case
                            
                        
                        
                                ++++++++ SANS PRETRAITEMENTS ++++++++++++++
                        
                - Sans Lemmmatization, sans stop words, sans lower case et LMDirichletSimilarity:
                        tmux new -s index_LMDirichletSimilarity_NoneSegmentation_keep-stop-words_no-lower-case
                        mkdir index_LMDirichletSimilarity_NoneSegmentation_keep-stop-words_no-lower-case
                        docker run -it --privileged -d --name index_LMDirichletSimilarity_NoneSegmentation_keep-stop-words_no-lower-case  -v $(pwd):/root/pylucene_workdir  coady/pylucene:latest
                        docker exec -it index_LMDirichletSimilarity_NoneSegmentation_keep-stop-words_no-lower-case bash
                        cd /root/pylucene_workdir
                        time  python3 indexing.py \
                           index_LMDirichletSimilarity_NoneSegmentation_keep-stop-words_no-lower-case \
                            LMDirichletSimilarity \
                            NoneSegmentation \
                            keep-stop-words \
                            no-lower-case
                        
                        
                        
            
    """
    parser = argparse.ArgumentParser()
    # parser.add_argument("query", help="the query to search for", type=str)
    # args = parser.parse_args()
    # query = args.query
    # indexer("TREC_AP_88-90/collection_de_documents/decompressed", "index")

    parser.add_argument(
        "index_dir", help="the directory where the index will be created", type=str
    )
    parser.add_argument(
        "evaluation_formula",
        help="the evaluation formula to use: TF-IDF, 1 for BM25,  LMDirichletSimilarity,  for ClassicSimilarity",
        type=str,
    )
    parser.add_argument(
        "segmentation_type",
        help="the segmentation type to use: lemmatization for lemmatization, stemming for stemming, no-segmentation for no segmentation",
        type=str,
    )
    parser.add_argument(
        "stops_words_usage",
        help="the usage of stop words: keep-stop-words for no usage, remove-stop-words for usage",
        type=str,
    )
    parser.add_argument(
        "lower_case_usage",
        help="the usage of lower case: no-lower-case for no usage, use-lower-case for usage",
        type=str,
    )
    args = parser.parse_args()
    index_dir = args.index_dir
    evaluation_formula = EvaluationFormula.from_str(args.evaluation_formula)
    segmentation_type = SegmentationType.from_str(args.segmentation_type)
    stop_word_text = args.stops_words_usage
    stops_words_usage = False
    if stop_word_text == "keep-stop-words":
        stops_words_usage = False
    elif stop_word_text == "remove-stop-words":
        stops_words_usage = True
    else:
        raise ValueError(
            "The value passed in parameter is not a valid stops_words_usage"
        )

    lower_case_text = args.lower_case_usage
    lower_case_usage = False
    if lower_case_text == "no-lower-case":
        lower_case_usage = False
    elif lower_case_text == "use-lower-case":
        lower_case_usage = True
    else:
        raise ValueError(
            "The value passed in parameter is not a valid lower_case_usage"
        )

    # Affichage des parametres passes en ligne de commande

    print(
        f"Parametres: \n \t- index_dir: {index_dir} \n \t- evaluation_formula: {evaluation_formula} \n \t- segmentation_type: {segmentation_type} \n \t- stops_words_usage: {stops_words_usage} \n \t- lower_case_usage: {lower_case_usage} \n"
    )
    # Creation d'un fichier avec le nom du dossier de l'index et la date et l'heure de creation qui contient les parametres passes en ligne de commande
    with open(
        f"indexation/{index_dir}_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.txt", "w"
    ) as f:
        f.write(
            f"Parametres: \n \t- index_dir: {index_dir} \n \t- evaluation_formula: {evaluation_formula} \n \t- segmentation_type: {segmentation_type} \n \t- stops_words_usage: {stops_words_usage} \n \t- lower_case_usage: {lower_case_usage} \n"
        )

        # dependement de l'agorithme de segmentation, le repertoire des documents change

    if segmentation_type == SegmentationType.Lematization:
        docdir = "TREC_AP_88-90/preprocessed_data/lemmatized"
    elif segmentation_type == SegmentationType.Stemming:
        docdir = "TREC_AP_88-90/preprocessed_data/stemmed"
    else:
        docdir = "TREC_AP_88-90/preprocessed_data/initial"
        
        
    index_dir = f"indexation/{index_dir}"
    ir = IR(
        docdir,
        index_dir,
        evaluation_formula,
        stops_words_usage,
        segmentation_type,
        lower_case_usage,
    )

    # On fait une recherche dans l'index avec la requete query
    # ir.search(query)


if __name__ == "__main__":
    indexing()
