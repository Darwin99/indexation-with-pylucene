import xml.etree.ElementTree as et
import copy
from config_properties import SegmentationType
import nltk
import spacy
from tqdm import tqdm
import glob
import os
import re

from lxml import etree

import pickle
from multiprocessing import Pool


# from symspellpy import SymSpell


def remove_stop_words_in_text(text):
    """
    Cette fonction va retirer les stops words du texte passe en parametre
    """
    # We will remove the stop words
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    return " ".join([token.text for token in doc if not token.is_stop])


def segment_words(text, segmentationType):
    """
    Cette fonction va segmenter les mots du texte passe en parametre
    """
    if segmentationType == SegmentationType.Stemming:
        # We will stem the words
        stemmer = nltk.stem.PorterStemmer()
        return " ".join([stemmer.stem(word) for word in text.split(" ")])
    elif segmentationType == SegmentationType.Lematization:
        # We will lemmatize the words
        nlp = spacy.load("en_core_web_sm")
        doc = nlp(text)
        return " ".join([token.lemma_ for token in doc])
    else:
        return text


# We are going to create a class for the IRdocument
class IRDocument:

    """
    A IRdocument object will take an xml document and parse each child of the document, create a property o and assigns its content to the property
    """

    def __init__(self, xml_doc):
        for child in xml_doc:
            # check if the atrribute exist
            if hasattr(self, child.tag):
                # if it exist, we will append the value to the property
                # self.child.tag += child.text

                try:
                    prop_value = getattr(self, child.tag)
                    if prop_value is not None:
                        setattr(self, child.tag, prop_value + " " + child.text)
                    # else:
                    #     setattr(self, child.tag, child.text)
                except TypeError as e:
                    print(f"Error in {child.tag} \n\n {e}")

                # setattr(self, child.tag, + child.text)
                # getattr(self,child.tag).append(child.text)
            else:
                # if it does not exist, we will create the property and assign the value to it
                setattr(self, child.tag, child.text)
        # La propriete text contient le contenu du document pour chaque attribut du document
        self.text = ""
        for prop in self.__dict__:
            if prop != "text":
                # print(f"{prop}\n")

                try:
                    prop_value = getattr(self, prop)
                    if prop_value is not None:
                        self.text = self.text + (prop_value + " ")
                except TypeError as e:
                    print(f"Error in {prop} \n\n {e}")
                    return

        self.text = self.text.lower()

    def __str__(self):
        """
        This method will return a string representation of the IRdocument

        for each property, we'll print the its value

        """
        string_IRdocument = "IRDocument\n"

        for prop in self.__dict__:
            try:
                prop_value = getattr(self, prop)
                if prop_value is not None:
                    string_IRdocument += prop + ": " + str(getattr(self, prop)) + "\n"
            except TypeError as e:
                print(f"Error in {prop} \n\n {e}")

        string_IRdocument += "\n\n"

        return string_IRdocument

    def preprocess(
        self,
        remove_stop_words=False,
        segmentationType=SegmentationType,
        lower_case=False,
    ):
        """
        Cette fonction va parcourir chaque propriete de l'objet IRDocument et retirer les stops words si le parametre remove_stop_words est a True
        Le retrait des stops words est fait avec la librairie spacy
        et segmenter les mots si le parametre segmentationType est different de NoneSegmentation
        Le stemming est fait avec la librairie nltk
        La lemmatization est faite avec la librairie spacy
        """

        if remove_stop_words:
            # On retire les stop words pour chaque   propriete de l'objet IRDocument
            for prop in self.__dict__:
                if prop != "text":
                    try:
                        prop_value = getattr(self, prop)
                        if prop_value is not None:
                            setattr(
                                self,
                                prop,
                                remove_stop_words_in_text(getattr(self, prop)),
                            )
                    except TypeError as e:
                        print(f"Error in {prop} \n\n {e}")
                    # setattr(self, prop, remove_stop_words_in_text(getattr(self, prop)))
        if segmentationType != SegmentationType.NoneSegmentation:
            # On segmente les mots pour chaque propriete de l'objet IRDocument
            for prop in self.__dict__:
                if prop != "text":
                    try:
                        prop_value = getattr(self, prop)
                        if prop_value is not None:
                            setattr(
                                self,
                                prop,
                                segment_words(getattr(self, prop), segmentationType),
                            )
                    except TypeError as e:
                        print(f"Error in {prop} \n\n {e}")

                    # setattr(
                    #     self, prop, segment_words(getattr(self, prop), segmentationType)
                    # )

        # mise a jour de la propriete text
        self.text = ""
        for prop in self.__dict__:
            if prop != "text":
                try:
                    prop_value = getattr(self, prop)
                    if prop_value is not None:
                        self.text = self.text + (prop_value + " ")
                except TypeError as e:
                    print(f"Error in {prop} \n\n {e}")
                    return
        if lower_case:
            # On va mettre juste l'attribut text en minuscule
            self.text = self.text.lower()

    def full_content(
        self,
        preprocess=False,
        remove_stop_words=False,
        segmentationType=SegmentationType.NoneSegmentation,
        lower_case=False,
    ):
        """
        Cette fonction retourne le contenu complet du document qui est la concatenation de toutes les proprietes

        """
        other_doc = None
        if preprocess:
            other_doc = copy.deepcopy(self)
            other_doc.preprocess(
                remove_stop_words=remove_stop_words,
                segmentationType=segmentationType,
                lower_case=lower_case,
            )
        else:
            other_doc = self
        # On retourne le contenu complet du document qui est la concatenation de toutes les proprietes
        for prop in other_doc.__dict__:
            if prop != "text":
                try:
                    prop_value = getattr(other_doc, prop)
                    if prop_value is not None:
                        other_doc.text = other_doc.text + (prop_value + " ")
                except TypeError as e:
                    print(f"Error in {prop} \n\n {e}")
                    return

        return other_doc.text


def full_content(
    initial_doc: IRDocument,
    remove_stop_words=False,
    segmentationType=SegmentationType,
    lower_case=False,
):
    """
    Cette fonction va parcourir chaque propriete de l'objet IRDocument et retirer les stops words si le parametre remove_stop_words est a True
    Le retrait des stops words est fait avec la librairie spacy
    et segmenter les mots si le parametre segmentationType est different de NoneSegmentation
    Le stemming est fait avec la librairie nltk
    La lemmatization est faite avec la librairie spacy


    """
    # on copy l'objet initial_doc dans un autre objet doc
    doc = copy.deepcopy(initial_doc)

    if remove_stop_words:
        # On retire les stop words pour chaque   propriete de l'objet IRDocument
        for prop in doc.__dict__:
            if prop != "text":
                try:
                    prop_value = getattr(doc, prop)
                    if prop_value is not None:
                        setattr(
                            doc, prop, remove_stop_words_in_text(getattr(doc, prop))
                        )
                except TypeError as e:
                    print(f"Error in {prop} \n\n {e}")

    if segmentationType != SegmentationType.NoneSegmentation:
        # On segmente les mots pour chaque propriete de l'objet IRDocument
        for prop in doc.__dict__:
            if prop != "text":
                try:
                    prop_value = getattr(doc, prop)
                    if prop_value is not None:
                        setattr(
                            doc,
                            prop,
                            segment_words(getattr(doc, prop), segmentationType),
                        )
                except TypeError as e:
                    print(f"Error in {prop} \n\n {e}")

    # On retourne le contenu complet du document qui est la concatenation de toutes les proprietes
    for prop in doc.__dict__:
        if prop != "text":
            try:
                prop_value = getattr(doc, prop)
                if prop_value is not None:
                    doc.text = doc.text + (prop_value + " ")
            except TypeError as e:
                print(f"Error in {prop} \n\n {e}")

    if lower_case:
        # On va mettre juste l'attribut text en minuscule
        doc.text = doc.text.lower()
    return doc.text


# The IRdocument don;t have a root
def extract_docs(file_name: str):
    """
    Extracts the IRdocuments from the XML file and returns a list of IRdocuments
    :param file_name: name of the XML file
    :return: list of IRdocuments
    """
    with open(file_name, "r", encoding="iso-8859-1") as file:
        content = file.read()
        # print(f"{content}")
        # on retire les caracteres html comme &amp; &lt; &gt; &apos; &quot;
        # content = html.unescape(content)
        # print(f"{content}")

        # check id it contents &equals;
        # content = content. replace('&', '&amp;')

        if len(content) == 0:
            print(f"File {file_name} is empty")
            return []
        content = '<?xml version="1.0"?>\n <root>' + content + "</root>"
        try:
            parser = etree.XMLParser(recover=True)  # recover from bad characters.
            root = etree.fromstring(content, parser=parser)
            # root = et.fromstring(content)
        except et.ParseError as e:
            print(f"File {file_name} is not a valid XML file\n\n{e}")
            return []

        docs = []
        for doc in root:
            docs.append(IRDocument(doc))
    # print(f"File {file_name} has {len(docs)} documents")
    return docs


class Topic:
    def __init__(self, num, title, desc):
        self.num = num
        self.title = title
        self.desc = desc

    def __repr__(self):  # cette
        return f"Topic(num={self.num}, title={self.title}, desc={self.desc})"

    def query(
        self,
        short_query=True,
        remove_stop_words=False,
        segmentationType=SegmentationType.NoneSegmentation,
        lower_case=False,
    ):
        """
        Cette fonction retourne la requete du topic
        """
        query_string = ""

        if short_query:
            # print("\n\nshort query\n\n")
            query_string = self.title
        else:
            # print("\n\nlong query\n\n")
            query_string = self.title + " " + self.desc

        # print(f"\nquery: {query_string}\n\n\n\n\n")


        if remove_stop_words:
            query_string = remove_stop_words_in_text(self.title)
        if segmentationType != SegmentationType.NoneSegmentation:
            query_string = segment_words(query_string, segmentationType)
        if lower_case:
            query_string = query_string.lower()
        

        print(f"Processed query: {query_string}\n\n\n\n\n")

        return query_string


# Example usage
# with open("TREC_AP_88-90/Topics-requetes/topics.1-50.txt", "r", encoding="iso-8859-1") as file:
#     content = file.read()
#     # xml_content = """...<your XML content here>..."""
#     content = '<?xml version="1.0"?>\n <root>' + content + "</root>"
#     topics = extract_topics(content)

#     # for i, topic in enumerate(topics, start=1):
#     #     print(f"Topic {i}:")
#     #     print(topic)
#     #     print("\n")
#     print(f"{len(topics)}\n\n")
#     # Affichage de tous les attributs du topic 0 et valeur
#     # for prop in topics[0].__dict__:
#     #     print(f"{prop}: {getattr(topics[0], prop)}\n\n\n")


# print(f"{topics[0].num}\n\n\n")


# docs = extract_docs("TREC_AP_88-90/collection_de_documents/decompressed/AP880212")
# first_doc = docs[0]
# print(f"len(docs) = {len(docs)}")
# print(f"{first_doc.__dict__}\n\n")
# print(f"{first_doc.text}\n\n")
# # print(f"{remove_stop_words_in_text(first_doc.text)}\n\n")
# # print(f"{segment_words(first_doc.text, SegmentationType.Lematization)}\n\n")
# # print(f"{full_content(first_doc, remove_stop_words=True)}\n\n")
# # print(f"{full_content(first_doc, remove_stop_words=True, segmentationType=SegmentationType.Lematization)}\n\n")
# # print(f"{full_content(first_doc, remove_stop_words=True, segmentationType=SegmentationType.Stemming)}\n\n")
# # print(f"{first_doc.full_content(preprocess=True)}\n\n")
# # print(f"{full_content(first_doc, remove_stop_words=True, segmentationType=SegmentationType.Lematization)}\n\n")
# print(
#     f"{first_doc.full_content(preprocess=True, remove_stop_words=True, segmentationType=SegmentationType.Lematization)}\n\n"
# )


# doc_dir = "TREC_AP_88-90/collection_de_documents/decompressed"

# nunmer_of_docs = 0
# for file_name in tqdm(glob.glob(os.path.join(doc_dir, "*"))):
#     docs = extract_docs(file_name)
#     sub_docs_number = len(docs)
#     nunmer_of_docs += sub_docs_number
#     print(f"{file_name} has {sub_docs_number} documents")
#     del docs
# print(f"\n\n\nTotal number of documents is {nunmer_of_docs}")


def extract_topic_object_from_topic_text(topic_text):
    """
    Cette fonction retourne un objet Topic a partir du texte du topic
    On ne s'interresse qu'au numero, titre et description du topic

    """

    # on va chercher le numero du topic en utilssant find
    num = topic_text[topic_text.find("<num>") + len("<num>") : topic_text.find("<dom>")]
    num = num.strip()
    #  on extraie le numero du topic
    num = num.split(":")[1].strip()
    # print(f"{num}\n")

    # On va chercher le titre du topic en utilisant find
    title = topic_text[
        topic_text.find("<title>") + len("<title>") : topic_text.find("<desc>")
    ]
    title = title.strip()
    # on extraie le titre du topic
    title = title.split(":")[1].strip()
    # print(f"{title}\n")

    # On va chercher la description du topic en utilisant find
    # par ce que l'on peut avoir apre la balise <desc> soit <narr> ou <smry>, on va chercher la balise la plus proche
    nearest_balise = topic_text.find("<smry>")
    if nearest_balise == -1:
        nearest_balise = topic_text.find("<narr>")
    desc = topic_text[topic_text.find("<desc>") + len("<desc>") : nearest_balise]
    desc = desc.strip()
    # on extraie la description du topic
    desc = desc.split(":")[1].strip()
    # print(f"{desc}\n")

    # On cree un objet Topic
    topic = Topic(num, title, desc)
    # print(f"{topic}\n\n\n\n\n\n\n\n\n\n")
    return topic


def extract_topics_from_text_file(file_name):
    """
    Cette finction extraie les topics du fichier texte en utilisant les expressions regulieres

    """

    with open(file_name, "r", encoding="iso-8859-1") as file:
        content = file.read()

        topics = []
        # On va chercher les topics
        regular_expression = r"<top>[\s\S]*?<\/top>"
        matches = re.findall(regular_expression, content)
        # print(f"matches: {len(matches)}\n\n\n\n\n\n\n\n\n\n")
        for match in matches:
            topics.append(extract_topic_object_from_topic_text(match))
        return topics


# with open(
#     "TREC_AP_88-90/Topics-requetes/topics.51-100.txt", "r", encoding="iso-8859-1"
# ) as file:
#     content = file.read()
#     extract_topics_from_text(content)


# topics = extract_topics_from_text_file("TREC_AP_88-90/Topics-requetes/topics.101-150.txt")
# i = 0
# number_of_equalities = 0
# topic : Topic
# for topic in topics: 
#     print(f"Topic {i}\n\n")
#     processed_short_query = topic.query(remove_stop_words=True, segmentationType=SegmentationType.Stemming, lower_case=True, short_query=True)
#     processed_long_query = topic.query(remove_stop_words=True, segmentationType=SegmentationType.Stemming, lower_case=True, short_query=False)
#     print(f"\nProcessed short query:{processed_short_query}\n\n\n\n\n\n\n\n\n\n")
#     print(f"\nProcessed long query: {processed_long_query}\n\n\n\n\n\n\n\n\n\n")

#     if processed_short_query == processed_long_query:
#         number_of_equalities += 1
#     i += 1

# print(f"Number of Equalities: {number_of_equalities}\n\n\n\n\n\n\n\n\n\n")
# print(f"Percentage of Equalities: {number_of_equalities/len(topics) * 100}\n\n\n\n\n\n\n\n\n\n")

def read_process_and_picle_docs_from_file(
    file_name,
    pickle_file_name,
    remove_stop_words=True,
    segmentationType=SegmentationType.Lematization,
    lower_case=True,
):
    """
    Cette fonction va lire le fichier file_name, extraire les documents, les traiter et les sauvegarder dans le fichier pickle_file_name
    """
    # affichange du numero du processus
    print(f"Process {os.getpid()} is working on {file_name}")
    docs = extract_docs(file_name)
    for doc in tqdm(docs):
        doc.preprocess(
            remove_stop_words=remove_stop_words,
            segmentationType=segmentationType,
            lower_case=lower_case,
        )
    with open(pickle_file_name, "wb") as file:
        pickle.dump(docs, file)
    return docs


def read_IRDocuments_from_pickle_file(pickle_file_name):
    """
    Cette fonction va lire le fichier pickle_file_name, extraire les documents et les retourner
    """
    with open(pickle_file_name, "rb") as file:
        docs = pickle.load(file)
    return docs


# read_process_and_picle_docs_from_file("TREC_AP_88-90/collection_de_documents/decompressed/AP880212", "TREC_AP_88-90/preprocessed_data/lemmatized/AP880212.pkl", remove_stop_words=True, segmentationType=SegmentationType.Lematization, lower_case=True)

# docs = read_IRDocuments_from_pickle_file("TREC_AP_88-90/preprocessed_data/lemmatized/AP880212.pkl")

# print(f"{len(docs)}\n\n\n\n\n\n\n\n\n\n")
# print(f"{docs[0].__dict__}\n\n\n\n\n\n\n\n\n\n")
# print(f"{docs[0].text}\n\n\n\n\n\n\n\n\n\n")

# # same with stemming
# read_process_and_picle_docs_from_file("TREC_AP_88-90/collection_de_documents/decompressed/AP880212", "TREC_AP_88-90/preprocessed_data/stemmed/AP880212.pkl", remove_stop_words=True, segmentationType=SegmentationType.Stemming, lower_case=True)
# docs_2 = read_IRDocuments_from_pickle_file("TREC_AP_88-90/preprocessed_data/stemmed/AP880213.pkl")


# print("Stemming\n\n\n\n\n\n\n\n\n\n")
# print(f"{len(docs_2)}\n\n\n\n\n\n\n\n\n\n")
# print(f"{docs_2[0].__dict__}\n\n\n\n\n\n\n\n\n\n")
# print(f"{docs_2[0].text}\n\n\n\n\n\n\n\n\n\n")


# docs = extract_docs("TREC_AP_88-90/collection_de_documents/decompressed/AP880212")
# first_doc = docs[0]

# print(f"Initial Doc: \n\n\n{first_doc.text}\n\n\n\n\n\n\n\n\n\n")

# first_doc.preprocess(
#     remove_stop_words=True,
#     segmentationType=SegmentationType.Lematization,
#     lower_case=True,
# )

# print(f"Preprocessed Doc: \n\n\n{first_doc.text}\n\n\n\n\n\n\n\n\n\n")


def process_and_pickle_docs_from_dir(
    dir_name,
    pickle_dir_name,
    remove_stop_words=True,
    segmentationType=SegmentationType.Lematization,
    lower_case=True,
    number_of_processes=4,
):
    """
    Cette fonction va utiliser un Pool avec multiprocessing
    pour chacun traiter un fichier du repertoire dir_name
    et le sauvegarder dans le repertoire pickle_dir_name avec le meme nom
    """
    # On cree le repertoire pickle_dir_name s'il n'existe pas
    if not os.path.exists(pickle_dir_name):
        os.makedirs(pickle_dir_name)

    # On cree un pool de processus
    # pool = Pool(processes=number_of_processes)

    # On va traiter chaque fichier du repertoire dir_name
    # for file_name in tqdm(glob.glob(os.path.join(dir_name, "*"))):
    #     # On va traiter le fichier file_name et le sauvegarder dans le repertoire pickle_dir_name avec le meme nom
    #     pool.apply_async(
    #         read_process_and_picle_docs_from_file,
    #         args=(
    #             file_name,
    #             os.path.join(
    #                 pickle_dir_name, os.path.basename(file_name).split(".")[0] + ".pkl"
    #             ),
    #             remove_stop_words,
    #             segmentationType,
    #             lower_case,
    #         ),
    #     )
    #        # # On ferme le pool
    #     pool.close()
    #     # On attend la fin de tous les processus
    #     pool.join()

    with Pool(processes=number_of_processes) as p:
        p.starmap(
            read_process_and_picle_docs_from_file,
            tqdm(
                [
                    (
                        file_name,
                        os.path.join(
                            pickle_dir_name,
                            os.path.basename(file_name).split(".")[0] + ".pkl",
                        ),
                        remove_stop_words,
                        segmentationType,
                        lower_case,
                    )
                    for file_name in glob.glob(os.path.join(dir_name, "*"))
                ]
            ),
        )


# process_and_pickle_docs_from_dir(
#     "TREC_AP_88-90/small_test_version",
#     "TREC_AP_88-90/preprocessed_data/lemmatized",
#     remove_stop_words=True,
#     segmentationType=SegmentationType.Lematization,
#     lower_case=True,
#     number_of_processes=4,
# )

# process_and_pickle_docs_from_dir(
#     "TREC_AP_88-90/small_test_version",
#     "TREC_AP_88-90/preprocessed_data/stemmed",
#     remove_stop_words=True,
#     segmentationType=SegmentationType.Stemming,
#     lower_case=True,
#     number_of_processes=4,
# )


def read_process_and_pickle_topics_from_file(
    file_name,
    pickle_file_name,
    remove_stop_words=True,
    segmentationType=SegmentationType.Lematization,
    lower_case=True,
):
    """
    Cette fonction va lire le fichier file_name, extraire les topics, les traiter et les sauvegarder dans le fichier pickle_file_name
    """
    # affichange du numero du processus
    print(f"Process {os.getpid()} is working on {file_name}")
    topics = extract_topics_from_text_file(file_name)
    for topic in tqdm(topics):
        if segmentationType != SegmentationType.NoneSegmentation:
            topic.title = segment_words(topic.title, segmentationType)
            topic.desc = segment_words(topic.desc, segmentationType)

        if remove_stop_words:
            topic.title = remove_stop_words_in_text(topic.title)
            topic.desc = remove_stop_words_in_text(topic.desc)
        if lower_case:
            topic.title = topic.title.lower()
            topic.desc = topic.desc.lower()
    with open(pickle_file_name, "wb") as file:
        pickle.dump(topics, file)
    return topics


def read_topics_from_pickle_file(pickle_file_name):
    """
    Cette fonction va lire le fichier pickle_file_name, extraire les topics et les retourner
    """
    with open(pickle_file_name, "rb") as file:
        topics = pickle.load(file)
    return topics


def process_and_pickle_topics_from_dir(
    dir_name,
    pickle_dir_name,
    remove_stop_words=True,
    segmentationType=SegmentationType.Lematization,
    lower_case=True,
    number_of_processes=4,
):
    """
    Cette fonction va utiliser un Pool avec multiprocessing
    pour chacun traiter un fichier du repertoire dir_name
    et le sauvegarder dans le repertoire pickle_dir_name avec le meme nom
    """
    # On cree le repertoire pickle_dir_name s'il n'existe pas
    if not os.path.exists(pickle_dir_name):
        os.makedirs(pickle_dir_name)

    with Pool(processes=number_of_processes) as p:
        p.starmap(
            read_process_and_pickle_topics_from_file,
            tqdm(
                [
                    (
                        file_name,
                        os.path.join(
                            pickle_dir_name,
                            os.path.basename(file_name).split(".")[0]
                            + os.path.basename(file_name).split(".")[1]
                            + ".pkl",
                        ),
                        remove_stop_words,
                        segmentationType,
                        lower_case,
                    )
                    for file_name in glob.glob(os.path.join(dir_name, "*"))
                ]
            ),
        )




# topics = read_topics_from_pickle_file("TREC_AP_88-90/preprocessed_data/topics/stemmed/topics.pkl")

# print(f"{len(topics)}\n\n\n\n\n\n\n\n\n\n")
# print(f"{topics[0].__dict__}\n\n\n\n\n\n\n\n\n\n")
