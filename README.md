# RECHERCHE D'INFORMATION AVEC PYLUCENE

# Table of Contents
   [RESUME](#resume) 
1. [Introduction](#introduction)
2. [Etat de l'art](#etat-de-lart)
3. [Methodologie](methodologie)
4. [Description des données et reccources utilisées](#description-des-données-et-reccources-utilisées)
5. [Evaluation et résultats](#evaluation-et-résultats)
6. [Challenges](#challenges)
7. [Conclusion](#conclusion)



## [RESUME](#resume)
Dans le cadre de ce projet, nous élaborons un moteur de recherche d'informations (Information Retrieval en anglais) en utilisant le moteur de recherche **`Lucene`**, avec l'interface Python **`Pylucene`** encapsulée dans un conteneur `Docker`. L'indexation sera effectuée sur la collection TREC-88-90, avec et sans prétraitement (Stemming, élimination des stopwords), en suivant les schémas de pondération TF*IDF, BM25 et LM Dirichlet. Le système sera testé sur des requêtes courtes et longues.


## [Introduction](#introduction)
Depuis de nombre années, la quantité de données disponibles sur le web a augmenté de manière exponentielle. Il est donc devenu difficile de trouver les informations pertinentes. C'est pourquoi les moteurs de recherche sont devenus indispensables pour trouver les informations pertinentes. De là, de nombreux modeles ont vus le jour. Les chercheurs ont tout d'abord opté pour un modèle basique qui comme le modèle booléen notemment 

Le but de ce projet est de mettre en oeuvre les techniques de recherche d'information vues en cours pour répondre à une question de recherche. Pour ce faire, nous avons utilisé la bibliothèque **`PyLucene`** qui est une interface python pour la bibliothèque Java `Lucene`.
![indexing](./images/IR_with_pylucene.drawio.png)

Pour cela, nous utilerons Docker pour créer un conteneur qui contient l'environnement de travail nécessaire pour exécuter le projet.
![indexing](./images/IR_with_pylucene_docker.drawio.png)

<!-- ![indexing](./images/IR_with_pylucene-steps.drawio.png) -->

## Methodologie

![indexing](./images/IR_with_pylucene_INDEXATION.drawio.png)

4. [Description des données et reccources utilisées](#description-des-données-et-reccources-utilisées)

La collection de données vient du concours TREC (Text REtrieval Conference) qui est une série d'ateliers annuels sur la recherche d'information. Le but de ce concours est de comparer les systèmes de recherche d'information en utilisant des collections de données standardisées. NOus utiliserons dans ce cas la collection TREC AP 88-90 qui contient 242918 documents journalistiques de l'Associated Press (AP) de 1988 à 1990. Les documents sont en anglais et sont au format XML.

<!-- to center the image, we do the following -->
<p align="center">
  <img src="./images/data.drawio.png" alt="TREC_AP_88-90" width="500"/>
</p>

### Format des documents

Les documents sont au format XML et ont la structure suivante:

```xml
<DOC>
<DOCNO> AP880212-0001 </DOCNO>
<FILEID>AP-NR-02-12-88 2344EST</FILEID>
<FIRST> ...</FIRST>
<SECOND>AM-Vietnam-Amnesty,0411</SECOND>
<HEAD>Reports Former Saigon Officials Released from Re-education Camp</HEAD>
<DATELINE>BANGKOK, Thailand (AP) </DATELINE>
<TEXT>
    .
    .
    .
</TEXT>
</DOC>


```

### Format des requêtes

```xml
<top>
<head> Head...
<num> Number: 001
<dom> Domain...
<title> Topic
<desc> Description
<narr> Narrative
<con> Concept(s)
<fac> Factor(s)
<def> Definition(s)
</top>
```

### Format des fichiers de jugement de pertinence

Le format de ces fichiers de jugement de pertinence suit le format de TRECEVAL. Chaque ligne contient 4 elements séparés par un espace:

- Le numéro de la requête
- Une constante 0
- Le numéro du document
- Le rang du document

### Ressource matérielles utilisées

Nous avons parallelisé le prétraitement des documents en utilisant le module `multiprocessing` de python. Nous avons utilisé 30 processus pour le prétraitement des documents (**~ 2 jours**). Nous avons utilisé le serveur calul.info.uqam.ca

![indexing](./images/stemming_processing_on_30_cpus.png)

## [Evaluation et résultats](https://faculty.washington.edu/levow/courses/ling573_SPR2011/hw/trec_eval_desc.htm)

Le fichier de résultats a le format suivant :

- `query_id`, `iter`, `docno`, `rank`, `sim`, `run_id`, séparés par des espaces.
  - L'identifiant de requête (`query_id`) est le numéro de la requête (par exemple, 136.6 ou 1894, selon l'année d'évaluation).
  - La constante `iter`, 0, est requise mais ignorée par `trec_eval`.
  - Les numéros de document sont des valeurs de chaîne telles que FR940104-0-00001 (trouvées entre les balises `<DOCNO>` dans les documents).
  - La similarité (`sim`) est une valeur décimale.
  - Le classement (`rank`) est un entier de 0 à 1000, requis mais ignoré par le programme.
  - `Runid` est une chaîne qui est imprimée avec la sortie.

![indexing](./images/trec_eval.png)

Les résultats de l'évaluation sont les suivants:

![indexing](./images/final_ir_results.png)






![indexing](./images/map_chart.png)

Nous pensons que ces resultats ne sont pas très satisfaisants car nous avons utilisé le stemming(en **~2 Jours**) pour la segmentation des mots. Nous pensons que la lemmatisation donnera de meilleurs résultats mais beaucoup plus lentement(qui devrait prendre **~3 jours**)

## [Challenges](#challenges)

Pendant le prétraitement, nous avons rencontré quelques problèmes qui sont résolus dans toutes les fonctions d'extraction des documents :

- Le fichier XML n'est pas bien formé car il n'y a pas d'élément racine. Nous devons donc ajouter un élément racine au fichier et la balise de schéma XML.
- Le fichier XML contient des caractères spéciaux comme &, nous devons donc les remplacer par leur code HTML correspondant à l'aide de la bibliothèque lxml.
- Certains balises contiennent des valeurs vides et certains documents ont des balises différentes des autres.
- Le type d'encodage du fichier XML n'est pas pris en charge par Python, nous devons donc le changer en iso-8859-1.

- En ce sui concerne la requête, nous avons rencontré les problèmes car le fichier n'est pas correctment formaté donc est géré comme du texte.

- **Parallélisation de l'indexation**

  ## Conclusion

  Nous avons indexé en utilisant Pylucene la collection TREC AP 88-90. Nous avons utilisé le stemming pour la segmentation des mots. Nous avons utilisé 30 processus pour le prétraitement des documents. Nous avons utilisé le serveur calul.info.uqam.ca. Nous avons obtenu les résultats suivants: 0 en tulisant par exemple TF-ID comme schema de pondération et au maximum 0.14 avec BM25 et TF-IDF. Nous pensons que ces resultats ne sont pas très satisfaisants car nous avons utilisé le stemming pour la segmentation des mots. Nous pensons que la lemmatisation donnera de meilleurs résultats mais beaucoup plus lentement.

## Packages installation

To install the packages, run the following command:

```bash
pip install -r requirements.txt
```

### Install **_Spacy_**

To download the spacy english model, run the following command:

```bash
python -m spacy download en_core_web_sm
```

## Preprocessing

### Files decompression

As in TREC competition compressed data files in `.gz` format, we have to decompress them. To do so, we have used the `gzip` library in de file `decompress_files.sh` . To decompress the files, run, firstly, give the execution permission to the file using the following command:

```bash
chmod +x decompress_files.sh
```

Then, run the following command:

```bash
./decompress_files.sh data_path
```

where `data_path` is the path to the data folder that contains the compressed files. like `TREC_AP_88-90/collection_de_documents`

### Documents extraction

All files has many documents with the following format:

```xml
<DOC>
<DOCNO> AP880212-0001 </DOCNO>
<FILEID>AP-NR-02-12-88 2344EST</FILEID>
<FIRST> ...</FIRST>
<SECOND>AM-Vietnam-Amnesty,0411</SECOND>
<HEAD>Reports Former Saigon Officials Released from Re-education Camp</HEAD>
<DATELINE>BANGKOK, Thailand (AP) </DATELINE>
<TEXT>
    .
    .
    .
</TEXT>
</DOC>
```

During the preprocessing, we have encountered some problems that are solved in all the functions for the documents extraction:

- The xml file is not well formed because there is no root element. So we have to add a root element to the file and the xml schema tag
- The xml file contains special characters like & then we have to replace them with their corresponding html code using the library lxml
- Some tag contains empty values and some documents have different tags than the others
- The encoding type of the xml file is not supported by python, so we have to change it to iso-8859-1

The function `extract_docs` defined in the file `extract_doc.py` is used to extract the documents from the xml file and return a list of `IRDocument`. Each `IRDocument` object containts each tag with the respective value in the XML format. It also has an attribute `text` that contains the concatenation of all the tags values.

### Documents preprocessing

The preprocessing of a documents includes the following steps:

- Remove stop words with `spacy` because it is more efficient than the `nltk` library.
- Make the segmentation. This can be Steaming with `nltk` or Lemmatization with `spacy`.
- Make lower case all the words.

Each `IRDocument` object has a function `prepocess` that takes those parameters and preprocess the text of the document. The default value is `lemmatization` because it is more efficient than the `stemming` and it gives better results.

The next part is to dump the preprocess documents in a file. To do so, we have used the `pickle` library. The function `process_and_pickle_docs_from_dir` defined in the file `extract_doc.py` takes the preprocess arguments listed above and the path to the directory that contains the xml files and the directory where the pickle files will be saved.

To process the documents and save them in the `TREC_AP_88-90/preprocessed_data/lemmatized` directory, run the following command:

```bash
        time python pre_process_docs.py --segmentation_type Lematization --number_of_processes 30
```

Check the file `pre_process_docs.py` for more details about the arguments.

## Queries extraction

During the querying, we have encountered some problems:

- The queries contents some special characters and we used the QueryParser.escape() method to escape them

## Indexing

The indexing is done using the `IndexWriter` class. The class `IR` defined in the file [`indexing.py`](indexing.py) takes the path to the directory that contains the pickle files and the path to the directory where the index will be saved.

```bash
 time  python3 indexing.py \
                            index_LMDirichletSimilarity_Stemming_remove-stop-words_use-lower-case \
                            LMDirichletSimilarity \
                            Stemming \
                            remove-stop-words \
                            use-lower-case

```

For more details about the arguments, check the file [`indexing.py`](indexing.py).

## Retrieval

The retreival is done by using lucene `IndexSearcher` class. The class `IRSearcher` defined in the file [`searcher.py`](searcher.py) takes the path to the directory that contains the index and the path to the directory where the results will be saved.
It also takes the similarity function, the segmentation type, the stop words removal and the lower case usage and spell checking usage as arguments.

````bash

time python searcher.py \
                                    --query_dir TREC_AP_88-90/Topics-requetes/ \
                                        --qrels_file TREC_AP_88-90/jugements-de-pertinence/qrels.1-50.AP8890.txt \
                                            --evaluation_formula TF-IDF \
                                                --segmentation_type Stemming \
                                                    --short_query \
                                                        --stops_words_usage \
                                                            --lower_case_usage
                                                          
````
