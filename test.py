import copy



class Test:
    
    def __init__(self):
        
        self.val = 0


def test_val(test):
    
    # test.val = 1
    test_copy = copy.deepcopy(test)
    test_copy.val = 1
    # print(test.val)
    
    return test_copy
def main():
    
    test1 = Test()
    test2 = test_val(test1)
    
    print(f" test1.val = {test1.val}\n test2.val = {test2.val}")


if __name__ == "__main__":
    main()