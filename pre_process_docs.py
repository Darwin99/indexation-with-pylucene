import argparse
from extract_doc import (
    process_and_pickle_docs_from_dir,
)
from config_properties import SegmentationType, EvaluationFormula


# process_and_pickle_docs_from_dir(
#     "TREC_AP_88-90/collection_de_documents/decompressed",
#     "TREC_AP_88-90/preprocessed_data/stemmed",
#     remove_stop_words=True,
#     segmentationType=SegmentationType.Stemming,
#     lower_case=True,
#     number_of_processes=30,
# )


# process_and_pickle_docs_from_dir(
#     "TREC_AP_88-90/collection_de_documents/decompressed",
#     "TREC_AP_88-90/preprocessed_data/lemmatized",
#     remove_stop_words=True,
#     segmentationType=SegmentationType.Lematization,
#     lower_case=True,
#     number_of_processes=30,
# )


def main():
    """
    On prendra en arcgument le type de segmentation et le nombre de processus a utiliser
    
    Des exemples d'execution:
        time python pre_process_docs.py --segmentation_type Lematization --number_of_processes 30
        time python pre_process_docs.py --segmentation_type Stemming --number_of_processes 30
        time python pre_process_docs.py --segmentation_type NoneSegmentation --number_of_processes 30
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--segmentation_type",
        help="the segmentation type to use",
        type=str,
        default="NoneSegmentation",
    )
    parser.add_argument(
        "--number_of_processes",
        help="the number of processes to use",
        type=int,
        default=1,
    )
    args = parser.parse_args()
    segmentation_type_str = args.segmentation_type
    segmentation_type = SegmentationType.from_str(segmentation_type_str)
    number_of_processes = args.number_of_processes

    if segmentation_type == SegmentationType.Lematization:
        process_and_pickle_docs_from_dir(
            "TREC_AP_88-90/collection_de_documents/decompressed",
            "TREC_AP_88-90/preprocessed_data/lemmatized",
            remove_stop_words=True,
            segmentationType=segmentation_type,
            lower_case=True,
            number_of_processes=number_of_processes,
        )

    elif segmentation_type == SegmentationType.Stemming:
        process_and_pickle_docs_from_dir(
            "TREC_AP_88-90/collection_de_documents/decompressed",
            "TREC_AP_88-90/preprocessed_data/stemmed",
            remove_stop_words=True,
            segmentationType=segmentation_type,
            lower_case=True,
            number_of_processes=number_of_processes,
        )
    else:
    #    On va prepocesser les documents sans segmentation, ni suppression des stop words, ni lower case et on va utiliser 10 processus et sauvegarder les documents dans le dossier preprocessed_data/initial
        process_and_pickle_docs_from_dir(
            "TREC_AP_88-90/collection_de_documents/decompressed",
            "TREC_AP_88-90/preprocessed_data/initial",
            remove_stop_words=False,
            segmentationType=segmentation_type,
            lower_case=False,
            number_of_processes=number_of_processes,
        )

if __name__ == "__main__":
    main()