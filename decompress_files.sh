#!/bin/sh
# This script decompresses all files of the given directory and puts them in a new directory

# Check if the user has provided a directory
if [ $# -eq 0 ]
then
    echo "Please provide a directory"
    exit 1
fi

# Check if the directory exists
if [ ! -d $1 ]
then
    echo "Directory $1 does not exist"
    exit 1
fi

# Check if the directory is empty
if [ ! "$(ls -A $1)" ]
then
    echo "Directory $1 is empty"
    exit 1
fi

# Check if the directory already exists
if [ -d $1/decompressed ]
then
    echo "Directory $1/decompressed already exists"
    exit 1
fi

# Create the directory
mkdir $1/decompressed

# Decompress all files
#the files are .gz
for file in $1/*.gz
do
    gunzip -c $file > $1/decompressed/$(basename $file .gz) #basaename is the name of the file without the extension the the decompressed file is the same name without the extension
done



