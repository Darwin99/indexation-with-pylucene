from enum import Enum

class EvaluationFormula(Enum):
    '''
        Cette class donne des enumerations pour les formules d'evaluation
    '''
    
    TFIDFSimilarity = 'TFIDFSimilarity'
    BM25Similarity = 'BM25Similarity'
    ClassicSimilarity = 'ClassicSimilarity'
    LMDirichletSimilarity = 'LMDirichletSimilarity'
    NoneSimilarity = 'NoneSimilarity'
    
    
    @staticmethod
    def from_str(value):
        '''
            Cette methode retourne l'enumeration correspondante a la chaine de caractere passee en parametre
        '''
        if value == 'TF-IDF':
            return EvaluationFormula.TFIDFSimilarity
        elif value == 'BM25':
            return EvaluationFormula.BM25Similarity
        elif value == 'ClassicSimilarity':
            return EvaluationFormula.ClassicSimilarity
        elif value == 'LMDirichletSimilarity':
            return EvaluationFormula.LMDirichletSimilarity
        elif value == 'NoneSimilarity':
            return EvaluationFormula.NoneSimilarity
        else:
            raise ValueError('The value passed in parameter is not a valid EvaluationFormula')
    
    


class SegmentationType(Enum):
    Lematization = 'Lematization'
    Stemming = 'Stemming'
    NoneSegmentation = 'NoneSegmentation'
    
    
    @staticmethod
    def from_str(value):
        '''
            Cette methode retourne l'enumeration correspondante a la chaine de caractere passee en parametre
        '''
        if value == 'Lematization':
            return SegmentationType.Lematization
        elif value == 'Stemming':
            return SegmentationType.Stemming
        elif value == 'NoneSegmentation':
            return SegmentationType.NoneSegmentation
        else:
            raise ValueError('The value passed in parameter is not a valid SegmentationType')